import java.util.Scanner;

/**
Escriba un algoritmo para determinar 
si un número ingresado por teclado es Fibonacci o no.  
 */
public class Fibonacci{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el numero que desea saber si pertenece o no a la secuencia fibonacci: ");

        int numero = sc.nextInt();
        int fib1 = 0;
        int fib2 = 1;
        int fibR = 0;
        int bandera = 0;
        for(int i=0 ; fibR <= numero ; i++){
            fibR = fib1;
            fib2 = fib1 + fib2;
            fib1 = fib2 - fib1;
            System.out.print(fibR +" ");
            if(fibR == numero){
                bandera = 1;
                
                System.out.println("\n"+ fibR + " Si es un numero fibonacci");
                break;
            }
        }
        if( bandera == 0){
            System.out.println("\n" +numero + " no pertenece a la secuencia fibonacci");
        }

    }
}
