import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/*Asumiendo que se tiene la siguiente estructura:  
Escriba un programa que permita representar en orden descendente el árbol genealógico. 
El resultado debe representarse así: 
 
Carlos                         Juan 
    Maria							Wilson 
        Carlos 1						  Mateo 
            Sofia					Diana 
    Hernan 
        Efrain 

 */
public class ArbolGenealogico {
    public static void main (String args[]){

        Integer[] reg = {1,2,3,4,5,6,7,8,9,10};
        String[] nom = {"Carlos","Juan","Maria","Carlos1","sofia","Hernan","wilson","Diana","Mateo","Efrain"};
        Integer[] pad = {0,0,1,3,4,1,2,2,7,6};

        List<Integer> registros = new ArrayList<Integer>(Arrays.asList(reg));
        List<String> nombres = new ArrayList<String>(Arrays.asList(nom));
        List<Integer> padres = new ArrayList<Integer>(Arrays.asList(pad));
        
        List<ArrayList<String>> arbolG = new ArrayList<>();
        int cont = 0;
        int Nregistro = 1;
        while(contarCeros(padres) !=0){
            ArrayList<String> jerarquia= new ArrayList<>();
            jerarquia.add(nombres.get(cont));
            arbolG.add(jerarquia);

            int tempNregistro;
            while(padres.contains(Nregistro)){
                tempNregistro = Nregistro;
                ArrayList<String> generacion = new ArrayList<>();
                //puede ser una funcion
                while(padres.contains(Nregistro)){
                    int indice = padres.indexOf(Nregistro);//indice de donde esta el hijo(maria)
                    generacion.add(nombres.get(indice));
                    Nregistro = registros.get(indice); //el numero q tiene el hijo en registros
                    padres.set(indice,-1); //-1 indica que ya pase por ese elemento,"borrandolo"
                }
                arbolG.add(generacion);
                Nregistro = tempNregistro;
            }
            
            padres.set(cont,-1); //-1 indica que ya pase por ese elemento,"borrandolo"
            cont++;
            Nregistro = registros.get(cont);
            //imprimir arbol genealogico del 1er padre
            mostrarArbolGenealogico(arbolG);
            System.out.println();
            arbolG.clear(); //dejo vacio para almacenar el nuevo arbol del otro padre

            
        }
        
        

    }
    static int contarCeros(List<Integer> padres){
        int cont = 0;
        for(Integer i : padres){
            if(i==0){
                cont++;
            }
        }
        return cont;
    }
    static void mostrarArbolGenealogico(List<ArrayList<String>> arbolG){
        for(ArrayList obj: arbolG){
            ArrayList<String> temp = obj; 
            for(String num : temp){
                System.out.print(num + " "); 
            }
            System.out.println(); 
        }
    }
}