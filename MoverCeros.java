/*Escriba un programa en java ó php que 
coloque todos los ceros al final de una lista de números.
*/
public class MoverCeros {
    
    public static void main(String[] args){
        int[] lista = {7,10,0,9,0,6,1,10,0,17};
        imprimirArray(lista);
        int numeroCeros = contarCeros(lista);
        while(numeroCeros!=0){
            for(int i=0 ; i<lista.length-1 ; i++){
                int temp;
                if(lista[i] == 0){
                    temp = lista[i];
                    lista[i] = lista[i+1];
                    lista[i+1] = temp;
                    
                }
            }
            imprimirArray(lista);
            numeroCeros--;
        }
    }

    public static void imprimirArray(int[] arreglo){
        for(int i=0;i<arreglo.length;i++)
        {
            System.out.print(arreglo[i]+" ");
        }
        System.out.println("");
    }
    
    public static int contarCeros(int[] arreglo){
        int cont = 0;
        for(int i=0;i<arreglo.length;i++){
            if(arreglo[i]==0){
                cont++;
            }
        }
        return cont;
    }
}

